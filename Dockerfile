# Common setup steps
FROM mcr.microsoft.com/dotnet/sdk:6.0
RUN apt-get update && apt-get install -y curl
RUN curl -fsSL https://deb.nodesource.com/setup_19.x | bash - 
RUN apt-get install -y nodejs
COPY . /DevOps-Course-Workshop-Module-09-CD-Learners
WORKDIR /DevOps-Course-Workshop-Module-09-CD-Learners
RUN dotnet build
WORKDIR /DevOps-Course-Workshop-Module-09-CD-Learners/DotnetTemplate.Web
RUN npm install && find ./node_modules/ ! -user root | xargs chown root:root
RUN npm run build
ENTRYPOINT dotnet run